import Vue from 'vue';
import Vuetify from 'vuetify/lib';

Vue.use(Vuetify);

export default new Vuetify({
    theme: {
        options: {
            customProperties: true,
        },
        themes: {
            light: {
                primary: '#00BCFA',
                secondary: "#FA7F00",
            },
            dark: {
                primary: '#00BCFA',
                secondary: "#FA7F00",
            },
        },
        dark: true
    },
    icons: {
        iconfont: 'mdi',
    },
});
