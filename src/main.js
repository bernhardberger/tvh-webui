import Vue from "vue";
// import './plugins/axios'
import axios from "axios";
import App from "./App.vue";
import router from "./router";
import store from "./store/store";
import vuetify from './plugins/vuetify';
import 'roboto-fontface/css/roboto/roboto-fontface.css'
import '@mdi/font/css/materialdesignicons.css'
import i18n from './i18n'
import './boot';
import vueNumeralFilterInstaller from 'vue-numeral-filter';
Vue.use(vueNumeralFilterInstaller, { locale: 'en-gb' });


import AppLayout from './App';




Vue.config.productionTip = false;

import VueNativeSocket from 'vue-native-websocket'

Vue.use(VueNativeSocket, 'ws://192.168.8.80:9981/comet/ws', {
    protocol: 'tvheadend-comet',
    reconnection: true,
    reconnectionAttempts: 5,
    reconnectionDelay: 3000,
    format: 'json',
    store: store
});




new Vue(
  Vue.util.extend(
      {
        router,
        store,
        vuetify,
        i18n,
        // render: h => h(App)
      },
      AppLayout,
  ),

).$mount("#app");
