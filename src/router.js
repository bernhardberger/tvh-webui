import Vue from "vue";
import Router from "vue-router";
import Home from "./views/Home.vue";
import LiveTv from "./views/LiveTv";

import Blank from "./layouts/LayoutBlank";
import Dashboard from "./layouts/LayoutDashboard";

import Login from "./views/Login";

import store from "./store/store"
import IdnodeAdmin from "./layouts/LayoutInternal";

import ClassesShow from "./views/idnode-admin/ClassesShow";
import ClassesList from "./views/idnode-admin/ClassesList";
import ApiPathsList from "./views/idnode-admin/ApiPathsList";

Vue.use(Router);


let raw_routes = [
    {
        path: '/dashboard',
        component: Dashboard,
        meta: {
            requiresAuth: true,
            title: "Dashboard"
        },
        children: [
            {
                path: "/",
                name: "home",
                component: Home,
                meta: {
                    title: "Dashboard"
                }
            },
            {
                path: "/home",
                redirect: "/dashboard"
            },
            {
                path: "/live-tv",
                name: "live-tv",
                component: () => import("./views/LiveTv"),
                meta: {
                    title: "Live TV"
                }
            },
            {
                path: "/live-tv/play/:uuid",
                name: "live-tv/play",
                component: () => import("./views/LiveTv"),
                meta: {
                    title: "Live TV"
                }
            },
            {
                path: "/info",
                name: "info",

                component: () => import("./views/Info.vue"),

                meta: {
                    title: "Info"
                },
            }

        ]
    },
    {
        path: '/idnode-admin',
        meta: {
            requiresAuth: true,
        },
        component: () => import("./layouts/LayoutInternal"),
        children: [
            {
                path: "classes/list",
                name: "ClassesList",
                component: () => import("./views/idnode-admin/ClassesList"),
                meta: {
                    title: "Classes"
                },
            },
            {
                path: "classes/show/:class",
                name: "ClassesShow",
                component: () => import("./views/idnode-admin/ClassesShow"),
            },
            {
                path: "api/paths/list",
                name: "ApiPathsList",
                component: () => import("./views/idnode-admin/ApiPathsList"),
                meta: {
                    title: "API Endpoints"
                },
            },
            {
                path: "forms/components",
                name: "FormComponents",
                component: () => import("./views/idnode-admin/FormComponents"),
                meta: {
                    title: "Form Components"
                },
            }
        ]
    }
];

let routes = [];

raw_routes.forEach(function (route) {
    route.meta = {
        requiresAuth: true
    };
    routes.push(route);
});

let loginRoute = {
    path: "/",
    redirect: "/dashboard",
    name: "blank",
    component: Blank,
    children: [
        {
            path: '/login',
            name: 'login',
            component: Login
        },
        {
            path: '/logout',
            name: 'logout',
            redirect: '/login'
        }
    ]
};

routes.push(loginRoute);

let router = new Router({
    mode: "history",
    base: process.env.BASE_URL,
    routes: routes,
});

// function isAuthenticated() {
//   return localStorage.getItem('user');
// }

router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.requiresAuth)) {
        if (store.getters.isAuthenticated) {
            next();
        } else {
            next({
                path: '/login',
                query: {
                    returnUrl: to.fullPath
                }
            })
        }
    } else {
        next();
    }
});


// Lazy-loads view components, but with better UX. A loading view
// will be used if the component takes a while to load, falling
// back to a timeout view in case the page fails to load. You can
// use this component to lazy-load a route with:
//
// component: () => lazyLoadView(import('@views/my-view'))
//
// NOTE: Components loaded with this strategy DO NOT have access
// to in-component guards, such as beforeRouteEnter,
// beforeRouteUpdate, and beforeRouteLeave. You must either use
// route-level guards instead or lazy-load the component directly:
//
// component: () => import('@views/my-view')
//
// function lazyLoadView(AsyncView) {
//     const AsyncHandler = () => ({
//         component: AsyncView,
//         // A component to use while the component is loading.
//         loading: require('./components/BaseLoading').default,
//         // Delay before showing the loading component.
//         // Default: 200 (milliseconds).
//         delay: 0,
//         // A fallback component in case the timeout is exceeded
//         // when loading the component.
//         error: require('./components/BaseLoading').default,
//         // Time before giving up trying to load the component.
//         // Default: Infinity (milliseconds).
//         timeout: 1,
//     });
//
//     return Promise.resolve({
//         functional: true,
//         render(h, { data, children }) {
//             // Transparently pass any props or children
//             // to the view component.
//             return h(AsyncHandler, data, children)
//         },
//     })
// }

export default router;
