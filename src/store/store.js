import Vue from 'vue'
import Vuex from 'vuex'
import auth from './modules/auth'
import comet from './modules/comet'
import connections from "./modules/connections";
import app from "./modules/app";
import raw from "./modules/raw";

Vue.use(Vuex);

const debug = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
    modules: {
        comet,
        app,
        auth,
        connections,
        raw,
    },
    strict: debug,
})
