import Vue from 'vue';

const COMET_ACCESS_UPDATE = 'accessUpdate'; //tvheadend.js
const COMET_DISKSPACE_UPDATE = 'diskspaceUpdate'; //tvheadend.js
const COMET_SAT_SERVER_IP_PORT = 'setServerIpPort'; //tvheadend.js
const COMET_LOGMESSAGE = 'logmessage'; //tvheadend.js
const COMET_EPG = 'epg'; //epgevent.js; epg.js; dvr.js
const COMET_TITLE = 'title'; //idnode.js
const COMET_MPEGTS_NETWORK = 'mpegts_network'; //mpegts.js
const COMET_SERVICEMAPPER = 'servicemapper'; //status.js
const COMET_INPUT_STATUS = 'input_status'; //status.js
const COMET_CONNECTIONS = 'connections'; //status.js
const COMET_SUBSCRIPTIONS = 'subscriptions'; //status.js

const state = {
    socket: {
        isConnected: false,
        message: '',
        reconnectError: false,
    },
    boxId: '',
    access: {
        addresss: '',
        admin: 0,
        chname_num: 0,
        chname_src: 0,
        cookie_expires: 0,
        date_mask: '',
        dvr: 0,
        freediskspace: 0,
        info_area: "login, storage, time",
        label_formatting: 0,
        quicktips: 0,
        theme: '',
        ticket_expires: 0,
        time: 0,
        totaldiskspace: 0,
        uilevel: '',
        useddiskspace: '',
        username: ''
    },
    subscriptions: [],
    logs: [],
    server: {
        ip: '',
        port: ''
    }
};


const getters = {
    access: state => state.access,
    subscriptions: state => state.subscriptions,
    boxId: state => state.boxId,
    logs: state => state.logs
};

const mutations = {
    SOCKET_ONOPEN(state, event) {
        Vue.prototype.$socket = event.currentTarget
        // console.log('websocket open')
        state.socket.isConnected = true
    },
    SOCKET_ONCLOSE(state, event) {
        // console.log('websocket closed')
        state.socket.isConnected = false
    },
    SOCKET_ONERROR(state, event) {
        console.error(state, event)
    },
    // default handler called for all methods
    SOCKET_ONMESSAGE(state, message) {
        // console.log(message);

        state.socket.message = message;

        if ('boxid' in message) {
            if (state.boxId && state.boxId !== message.boxid) {
                // window.location.reload(); //TODO: find out why
            }
            state.boxId = message.boxid;
        }

        message.messages.forEach(function (m) {

            if (m.notificationClass === COMET_ACCESS_UPDATE) {
                state.access = m;
                // console.log(state)
            } else if (m.notificationClass === COMET_SUBSCRIPTIONS) {
                if (m.reload === 1) {
                    state.subscriptions = [];
                    return;
                }

                const item = state.subscriptions.find(item => item.id === m.id);

                if (item !== undefined) {
                    Object.assign(item, m);
                } else {
                    state.subscriptions.push(m);
                }
            } else if (m.notificationClass === COMET_DISKSPACE_UPDATE) {
                state.access.freediskspace = m.freediskspace;
                state.access.useddiskspace = m.useddiskspace;
                state.access.totaldiskspace = m.totaldiskspace;
            } else if (m.notificationClass === COMET_LOGMESSAGE) {
                state.logs.push(m.logtxt);
            } else if (m.notificationClass === COMET_SAT_SERVER_IP_PORT) {
                state.server.ip = m.ip;
                state.server.port = m.port;
            }
        });
    },
    // mutations for reconnect methods
    SOCKET_RECONNECT(state, count) {
        console.info(state, count)
    },
    SOCKET_RECONNECT_ERROR(state) {
        state.socket.reconnectError = true;
    },
};

function cometParse(state, message) {

}

const actions = {
    parseMessage(context, message) {
        if ('boxid' in message) {
            if (state.boxId && state.boxId !== message.boxid) {
                // window.location.reload(); //TODO: find out why
            }

            state.boxId = message.boxid;
        }
    }
};

export default {
    state,
    getters,
    actions,
    mutations,
}
