/* eslint-disable promise/param-names */

export default {
    namespaced: true,
    state: {
        loadingStack: [],
        loadingText: '',
    },
    getters: {
        isLoading: state => state.loadingStack.length > 0,
        loadingStack: state => state.loadingStack
    },
    mutations: {
        START_LOADING(state, what) {

            state.loadingStack.push({
                id: what
            });
        },

        DONE_LOADING(state, what) {
            const indexDone = state.loadingStack.findIndex(el => el.id === what);

            if (indexDone > -1) {
                state.loadingStack.splice(indexDone, 1);
            }
            // state.loadingStack.pop();
        }
    },
    actions: {
        startLoading({commit}, what) {
            commit('START_LOADING', what);
            console.log('start loading');
        },

        doneLoading({commit}, what) {
            commit('DONE_LOADING', what);
            console.log('end loading')
        }
    }
}
