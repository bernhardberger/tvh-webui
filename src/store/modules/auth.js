/* eslint-disable promise/param-names */

// import {USER_REQUEST} from '../actions/user'
import tvh from '../../services/tvh'

const state = {
    httpBasic: localStorage.getItem('http-basic') || '',
    status: '',
    hasLoadedOnce: false
};

const getters = {
    isAuthenticated: state => {
        console.log(state);
        return !!state.httpBasic
    },
    authStatus: state => state.status,
};

const actions = {
    authRequest: ({commit, dispatch}, connection) => {
        return new Promise((resolve, reject) => {
            commit('AUTH_REQUEST');

            tvh.login(connection.host, connection.username, connection.password)
                .then(resp => {
                    const httpBasic = btoa(connection.username + ':' + connection.password);

                    tvh.$axios.defaults.headers.common['Authorization'] = 'Basic ' + httpBasic;
                    tvh.$axios.defaults.baseURL = connection.host;

                    const connectionInfo = {
                        httpBasic: httpBasic,
                        host: connection.host
                    };

                    commit('AUTH_SUCCESS', connectionInfo);
                    // dispatch(USER_REQUEST)
                    resolve(resp)
                })
                .catch(err => {
                    commit('AUTH_ERROR', err);
                    reject(err)
                })
        })
    },
    authLogout: ({commit, dispatch}) => {
        return new Promise((resolve, reject) => {
            commit('AUTH_LOGOUT');
            resolve()
        })
    }
};

const mutations = {
    AUTH_REQUEST: (state) => {
        state.status = 'loading'
    },
    AUTH_SUCCESS: (state, resp) => {
        state.status = 'success';
        state.httpBasic = resp.httpBasic;
        state.host = resp.host;
        state.hasLoadedOnce = true;
    },
    AUTH_ERROR: (state) => {
        state.status = 'error';
        state.hasLoadedOnce = true;
    },
    AUTH_LOGOUT: (state) => {
        state.httpBasic = ''
    }
};

export default {
    state,
    getters,
    actions,
    mutations,
}
