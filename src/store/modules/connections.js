/* eslint-disable promise/param-names */

export const CONNECTIONS_ADD = 'CONNECTIONS_ADD';
export const CONNECTIONS_DELETE = 'CONNECTIONS_DELETE';
export const CONNECTIONS_LOAD = 'CONNECTIONS_LOAD';

import localStorageService from "../../services/localStorageService";


const state = {
    connections: []
};

const getters = {
    connections: state => {
        return state.connections
    },
};


const mutations = {
    [CONNECTIONS_LOAD]: (state, connections) => {
        state.connections = connections;
    },

    [CONNECTIONS_ADD]: (state, newConnection) => {
        state.connections.push(newConnection);
    },

    [CONNECTIONS_DELETE]: (state, connectionIndex) => {
        state.connections.splice(connectionIndex, 1);
    },
};


const actions = {
    'connectionAdd': ({commit, dispatch}, newConnection) => {
        commit(CONNECTIONS_ADD, newConnection);
        dispatch('connectionsSave');
    },

    'connectionDelete': ({commit, dispatch}, connection) => {
        commit(CONNECTIONS_DELETE, connection);
        dispatch('connectionsSave');
    },

    // [CONNECTIONS_UPDATE]({commit, dispatch}, connection) {
    // commit(CONNECTIONS_UPDATE, )
    // },

    'connectionsLoad': ({state, commit}) => {
        let connections = localStorageService.get('connections') || [];
        commit(CONNECTIONS_LOAD, connections);
    },

    async connectionsSave({state}) {
        localStorageService.save('connections', state.connections);
    }
};

export default {
    state,
    getters,
    actions,
    mutations,
}
