/* eslint-disable promise/param-names */

import tvh from "../../services/tvh";
import Vue from 'vue';

export default {
    namespaced: true,

    state: {
        classes: []
    },
    getters: {
        classes: state => state.classes
    },
    mutations: {
        UPDATE_CLASSES(state, classes) {
            state.classes = classes;
        },

        UPDATE_CLASS(state, payload) {
            const index = state.classes.findIndex(el => el.className === payload.className);
            Vue.set(state.classes[index], 'entries', payload.entries);
        },

        UPDATING_CLASS_START(state, className) {
            const index = state.classes.findIndex(el => el.className === className);
            Vue.set(state.classes[index], 'updating', true);
        },

        UPDATING_CLASS_END(state, className) {
            const index = state.classes.findIndex(el => el.className === className);
            Vue.set(state.classes[index], 'updating', false);
        }
    },
    actions: {
        async loadClasses({commit}) {

            try {
                const classes = [];
                let response = await tvh.$axios.get('/api/classes');

                Object.keys(response.data).forEach((key) => {
                    classes.push(
                        {
                            className: key,
                            title: response.data[key],
                            updating: false,
                            entries: []
                        }
                    );
                });

                commit('UPDATE_CLASSES', classes);
            } finally {

            }
        },

        async loadClass({commit}, className) {
            // this.dispatch('app/startLoading', className);
            commit('UPDATING_CLASS_START', className);

            try {
                let response= tvh.$axios.get('/api/raw/export?class=' + className);
                commit('UPDATE_CLASS', {
                    className: className,
                    entries: response.data
                });
            } finally {
                commit('UPDATING_CLASS_END', className);
            }
        },
    }
}
