import axios from 'axios';

export default {
    access: {
        entry: {
            getClass: () => axios.get('/access/entry/class'),
        }
    },

    serverinfo: {
        get: () => axios.get('/serverinfo')
    },

    pathlist: {
        get: () => axios.get('/pathlist')
    },

    raw: {
        getByClassName: className => axios.get('/raw/export?class=' + className),
        getByUuid: uuid => axios.get('/raw/export?uuid=' + uuid)
    }
}
