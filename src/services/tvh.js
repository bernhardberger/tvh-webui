import axios from "axios";
import router from "../router";
import store from "../store/store"

const $axios = axios.create();

$axios.interceptors.request.use(
    config => {
        return config;
    },

    error => {
        console.log('interceptor error');
        store.dispatch('authLogout');
        redirectToLogin();
        return Promise.reject(error);
    }
);

export default {
    login,
    $axios
}

function redirectToLogin() {
    router.push('/login').catch(() => {});
}

function login(server, username, password) {
    const request = $axios.get(
        `${server}/api/serverinfo`,
        {
            auth: {
                username: username,
                password: password
            }
        }
    );

    return new Promise((resolve, reject) => {
        request
        .then(response => {
                resolve(response)
            },
        )
        .catch(reason => {
            reject(reason);
        });
    });
}
