const get = key => {
    if (localStorage.getItem(key)) {
        try {
            return JSON.parse(localStorage.getItem(key));
        } catch (e) {
            console.log('Failed to get item from localStorage. Key: ' + key);
            return null;
        }
    } else {
        return null;
    }
};

const save = (key, value) => {
    localStorage.setItem(key, JSON.stringify(value));
};


export default {
    get,
    save
}