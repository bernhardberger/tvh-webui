import VCheckbox from "vuetify/lib/components/VCheckbox/VCheckbox";
import VSimpleCheckbox from "vuetify/lib/components/VCheckbox";
import VBtn from "vuetify/lib/components/VBtn";

export default {
    name: 'tvh-idnode-form-field',
    functional: true,

    props: {
        property: {
            type: Object,
            required: true
        }
    },

    render: function (createElement, context) {
        let element = createElement(
            'div', 'Unable to render property'
        );

        let property = context.props.property;

        let disabled = property.rdonly || false;

        if (property['enum']) {
            if (property.lorder) {
                // ordered list
            } else {
                // checkboxes
                if (property.list) {
                    // lovCombo ?
                } else {
                    // any combo - with typeahead -> chips?
                }
            }
        } else {
            switch (property.type) {
                case 'bool':
                    element = createElement(
                        VCheckbox, {
                            props: {
                                label: 'Test',
                            }
                        }
                    );
                    break;
                case 'time':
                    if (!property.duration) {

                    } else {
                        //no idea..
                    }
                    break;
                case 'int':
                case 'u32':
                case 'u16':
                case 's64':
                case 'dbl':
                    if (property.hexa) {
                        //hexadecimal
                    } else if (property.intsplit) {
                        // no idea..? maskRe: /[0-9\.]/
                    } else if (property.intmin || property.intmax) {
                        // spinner, intmin = minvalue, intmax = maxvalue, intstep = intervalstep || default 1
                    } else {
                        // normal number field
                    }
                    break;
                case 'perm':
                    break;

                default:
                // textfield|textarea
            }
        }

        return element;
    }
}
